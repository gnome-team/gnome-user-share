gnome-user-share (47.2-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 02 Dec 2024 11:25:14 -0500

gnome-user-share (47.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 16 Sep 2024 11:24:24 -0400

gnome-user-share (47~alpha-1) unstable; urgency=medium

  * New upstream release
  * Add debian/upstream/metadata
  * Bump minimum glib to 2.74
  * Bump Standards Version to 4.7.0
  * debian/docs: README → README.md
  * Refresh patches
  * Stop using debian/control.in & dh-sequence-gnome

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 10 Jul 2024 15:37:30 -0400

gnome-user-share (43.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 19 Sep 2022 15:36:49 -0400

gnome-user-share (43~alpha-1) unstable; urgency=medium

  * New upstream release
  * Stop building nautilus extension (Closes: #1014933)
  * debian/control.in: Bump minimum glib and meson
  * debian/control.in: Set Rules-Requires-Root: no
  * Drop meson patch: applied in new release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 15 Aug 2022 14:16:01 -0400

gnome-user-share (3.34.0-5) unstable; urgency=medium

  * Cherry-pick patch to fix build with latest meson (Closes: #1005590)
  * Bump debhelper-compat to 13

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 21 Mar 2022 14:00:10 -0400

gnome-user-share (3.34.0-3) unstable; urgency=medium

  * debian/rules: Set -Dsystemduserunitdir instead of build-depending on
    systemd, that should help fix FTBFS on non-linux architectures
  * debian/control.in: Bump Standards-Version to 4.6.0 (no further changes)

 -- Laurent Bigonville <bigon@debian.org>  Sun, 12 Sep 2021 11:37:53 +0200

gnome-user-share (3.34.0-2) unstable; urgency=medium

  * Stop overriding libexecdir
  * Build-Depend on dh-sequence-gnome instead of gnome-pkg-tools
  * Upload to unstable (Closes: #884777)

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 28 Sep 2019 09:04:27 -0400

gnome-user-share (3.34.0-1) experimental; urgency=medium

  * New upstream release
  * Build with meson
  * control: drop build-dep on gnome-common Closes: #829914
  * d/p/20-build-Don-t-install-systemd-user-unit-files-if-targe.patch: Dropped, gnome-user-share requires systemd --user session now

 -- Tim Lunn <tim@feathertop.org>  Sun, 22 Sep 2019 11:08:33 +1000

gnome-user-share (3.32.0.1-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 11 Mar 2019 19:38:17 -0400

gnome-user-share (3.32.0-1) experimental; urgency=medium

  * New upstream release
  * Drop obsolete Build-Depends on intltool

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 07 Mar 2019 05:19:47 -0500

gnome-user-share (3.28.0-2) unstable; urgency=medium

  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Restore -Wl,-O1 to our LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 25 Dec 2018 19:37:01 -0500

gnome-user-share (3.28.0-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * Update Vcs fields for migration to https://salsa.debian.org/
  * Enable all hardening flags
  * Drop -O1 from LDFLAGS
  * Print deleted *.la files

  [ Andreas Henriksson ]
  * New upstream release.
  * Bump nautilus-extension build-dep to >= 3.27.90

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 04 Sep 2018 19:01:16 +0200

gnome-user-share (3.18.3-3) unstable; urgency=medium

  * Update Vcs fields for conversion to git
  * Add debian/gbp.conf
  * Bump Standards-Version to 4.1.2
  * Bump debhelper compat to 11

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 16 Dec 2017 15:02:15 -0500

gnome-user-share (3.18.3-2) unstable; urgency=medium

  * Fully drop Bluetooth from packaging, obsolete since 3.18.0.
  * Bump Standards-Version to 4.1.0

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 03 Sep 2017 10:28:16 -0400

gnome-user-share (3.18.3-1) unstable; urgency=medium

  * New upstream release.
  * Drop 30-build-Properly-expand-paths-in-gnome-user-share-webd.patch, merged
    upstream.
  * Bump debhelper compat level to 10.
  * Use non-multiarch path (/usr/lib/gnome-user-share) for libexecdir.

 -- Michael Biebl <biebl@debian.org>  Tue, 20 Sep 2016 12:48:27 +0200

gnome-user-share (3.18.2-1) unstable; urgency=medium

  * New upstream release.
  * Add Build-Depends on systemd on Linux to get the correct path for the
    systemd user unit directory.
  * Add patch to ensure systemd is not mandatory so we can build on non-Linux
    architectures.
  * Bump Standards-Version to 3.9.8.
  * Drop debian/dirs, no longer needed.
  * Convert from cdbs to dh.
  * Bump debhelper compatibility level to 9.
  * Add patch to properly expand paths in gnome-user-share-webdav.desktop.

 -- Michael Biebl <biebl@debian.org>  Fri, 02 Sep 2016 18:16:19 +0200

gnome-user-share (3.18.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.7

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 24 Mar 2016 16:12:22 +0100

gnome-user-share (3.18.0-1) unstable; urgency=medium

  * New upstream release.
    - drops ObexPush support which moved to gnome-bluetooth.
  * debian/rules: drop moving of obexpush autostart files.

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 25 Sep 2015 10:15:56 +0200

gnome-user-share (3.14.2-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 12 Jun 2015 12:24:27 +0200

gnome-user-share (3.14.0-2) unstable; urgency=medium

  * debian/control.in: Depends against bluez-obexd instead of
    obex-data-server, gnome-user-share now uses the new interfaces

 -- Laurent Bigonville <bigon@debian.org>  Fri, 28 Nov 2014 12:53:48 +0100

gnome-user-share (3.14.0-1) unstable; urgency=medium

  * New upstream release.
  * Upload to unstable.

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 22 Sep 2014 21:05:23 +0200

gnome-user-share (3.13.91-1) experimental; urgency=medium

  * New upstream development release.
    - fixes build without bluetooth (eg. on kfreebsd & hurd).

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 13 Sep 2014 13:32:24 +0200

gnome-user-share (3.13.2-1) experimental; urgency=medium

  * New upstream development release.
    - Note: no longer started from gnome-session, now from g-s-d sharing.
  * Drop debian/patches/11_apache_noauth.patch
    - now included in upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 06 Sep 2014 10:33:02 -0700

gnome-user-share (3.10.2-1) unstable; urgency=medium

  * New upstream release.
  * Update build-dependencies according to configure.ac changes:
    - drop libdbus-glib-1-dev
    - bump libgnome-bluetooth-dev to >= 3.9.3
  * Bump gnome-bluetooth dependency to also be >= 3.9.3 as the build-dep.
  * Drop debian/patches/02_bluetooth_optional.patch
    - configure option --disable-bluetooth now available in upstream release.
  * Add gnome-common build-dependency for autoreconf to succeed.
  * Bump libgnome-bluetooth-dev to >= 3.12.0-4~
    - this version fixes missing dependency on libudev-dev (avoids FTBFS)
  * Bump Standards-Version to 3.9.5

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 15 Jul 2014 18:45:40 +0200

gnome-user-share (3.8.3-1) unstable; urgency=low

  * New upstream release.
  * Refresh 02_bluetooth_optional.patch.
  * Bump Standards-Version to 3.9.4. No further changes.

 -- Michael Biebl <biebl@debian.org>  Fri, 23 Aug 2013 23:35:21 +0200

gnome-user-share (3.8.0-2) unstable; urgency=low

  [ Emilio Pozuelo Monfort ]
  * debian/control.in:
    + Remove obsolete build dependency on scrollkeeper.
    + Switch apache2.2-bin dependency to apache2-bin for the Apache 2.4
      transition. Closes: #669729.
    + Bump minimum dependency on libapache2-mod-dnssd to 0.6-3.1~
      to get Apache 2.4 support.
  * debian/patches/10_apache_2.4.patch:
    + Don't load mod_unixd.so as it is statically linked.
      Thanks Arno Töll for the insights.

  [ Josselin Mouette ]
  * 11_apache_noauth.patch: fix the configuration with
    require_password=never. Thanks Arno Töll.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Fri, 07 Jun 2013 01:16:38 +0200

gnome-user-share (3.8.0-1) experimental; urgency=low

  * New upstream release.
    + debian/control.in:
      - Update build dependencies.
    + debian/patches/02_bluetooth_optional.patch:
      - Updated.
    + debian/rules:
      - Pass --disable-bluetooth to configure on !linux.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Sun, 31 Mar 2013 18:29:51 +0200

gnome-user-share (3.0.2-1) unstable; urgency=low

  [ Sebastien Bacher ]
  * debian/rules: disable scrollkeeper

  [ Sjoerd Simons ]
  * New upstream release
  * debian/patches/02_bluetooth_optional.patch
    + Updated

  [ Michael Biebl ]
  * Drop Build-Depends on libunique-3.0-dev.
  * Bump Build-Depends on libglib2.0-dev to (>= 2.28.0).
  * Update debian/copyright, drop outdated section about md5.[ch].
  * Update Vcs-* URLs.
  * Bump Standards-Version to 3.9.3.

 -- Michael Biebl <biebl@debian.org>  Tue, 29 May 2012 19:09:09 +0200

gnome-user-share (3.0.1-1) unstable; urgency=low

  * New upstream release.
  * debian/watch:
    - Track .xz tarballs.
  * debian/control.in:
    - Set pkg-gnome-maintainers@lists.alioth.debian.org as Maintainer.

 -- Michael Biebl <biebl@debian.org>  Wed, 19 Oct 2011 01:40:40 +0200

gnome-user-share (3.0.0-3) unstable; urgency=low

  [ Martin Pitt ]
  * debian/control.in: Add Vcs-* fields.

  [ Michael Biebl ]
  * Upload to unstable.
  * Bump debhelper compatibility level to 8.

 -- Michael Biebl <biebl@debian.org>  Thu, 13 Oct 2011 21:23:09 +0200

gnome-user-share (3.0.0-2) experimental; urgency=low

  * Require libnotify 0.7.

 -- Josselin Mouette <joss@debian.org>  Wed, 01 Jun 2011 14:26:00 +0200

gnome-user-share (3.0.0-1) experimental; urgency=low

  * New upstream release.
  * Switch to 3.0 quilt format.
  * Use dh-autoreconf.
  * Drop 90_relibtoolize.patch accordingly.
  * 02_bluetooth_optional.patch: updated for the new version.
  * Update build dependencies.
  * preinst: dropped, the fixed version is in stable.

 -- Josselin Mouette <joss@debian.org>  Tue, 31 May 2011 23:21:38 +0200

gnome-user-share (2.30.2-1) unstable; urgency=low

  * New upstream release.
    - Don't use deprecated libnotify API. Closes: #630278
  * Bump Build-Depends on libnotify-dev to (>= 0.7.0).
  * Use dh-autoreconf to update the build system.
    - Drop debian/patches/90_relibtoolize.patch.
    - Add Build-Depends on dh-autoreconf.
    - Add autoreconf.mk include to debian/rules.
  * Switch to dpkg source format 3.0 (quilt).
    - Drop Build-Depends on quilt.
    - Add debian/source/format.
    - Remove patchsys-quilt.mk include from debian/rules.
  * Bump Standards-Version to 3.9.2. No further changes.
  * debian/watch: Track .bz2 tarballs.

 -- Michael Biebl <biebl@debian.org>  Tue, 02 Aug 2011 07:29:53 +0200

gnome-user-share (2.30.1-1) unstable; urgency=low

  * Drop type-handling usage. Closes: #587876.
  * Bump standards version accordingly.
  * New upstream translation and bugfix release.
  * 03_NAUTILUS_IS_USER_SHARE.patch: dropped, merged upstream.
  * 90_relibtoolize.patch: regenerated.

 -- Josselin Mouette <joss@debian.org>  Wed, 20 Oct 2010 00:06:37 +0200

gnome-user-share (2.30.0-1) unstable; urgency=low

  [ Emilio Pozuelo Monfort ]
  * debian/watch:
    - Only track stable releases.

  [ Luca Falavigna ]
  * New upstream release.
  * Refresh patches for new upstream release:
    - 02_bluetooth_optional.patch
    - 90_relibtoolize.patch
  * debian/patches/03_makefile_error.patch:
    - Removed, fixed upstream.
  * debian/patches/03_NAUTILUS_IS_USER_SHARE.patch:
    - Fix use of undefined macro NAUTILUS_IS_SHARE.
  * debian/control.in:
    - Build-depend on libnautilus-extension-dev.
    - Bump Standards-Version to 3.8.4, no changes required.
  * debian/rules:
    - Do not consider libnautilus-share-extension a shared library.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 04 Apr 2010 18:48:44 +0200

gnome-user-share (2.28.2-4) unstable; urgency=low

  * debian/patches/02_bluetooth_optional.patch:
    - Re-add HAVE_BLUETOOTH macro, erroneously removed (Closes: #563614).
  * debian/patches/90_relibtoolize.patch:
    - Regenerate to pick the above change.

 -- Luca Falavigna <dktrkranz@debian.org>  Mon, 18 Jan 2010 00:33:29 +0100

gnome-user-share (2.28.2-3) unstable; urgency=low

  * debian/control.in:
    - adding back obex-data-server depends since g-u-s
      actually needs it and not obexd-server itself. (there's
      no GNOME package using obexd-server at the moment)

 -- Andrea Veri <and@debian.org>  Fri, 15 Jan 2010 14:40:50 +0100

gnome-user-share (2.28.2-2) unstable; urgency=low

  * debian/patches/90_relibtoolize.patch:
    - Regenerate it from scratch, previous attempt was unsuccessful and
      caused build failures on some ports.

 -- Luca Falavigna <dktrkranz@debian.org>  Wed, 23 Dec 2009 00:10:27 +0100

gnome-user-share (2.28.2-1) unstable; urgency=low

  [ Josselin Mouette ]
  * Depend on obexd-server, not obex-data-server.

  [ Luca Falavigna ]
  * New upstream bugfix release.
  * Refresh patches for new upstream release.
  * debian/control:
    - Add libunique-dev to Build-Depends.

 -- Luca Falavigna <dktrkranz@debian.org>  Tue, 22 Dec 2009 21:18:06 +0000

gnome-user-share (2.28.1-3) unstable; urgency=low

  * Oops, it’s not+linux-gnu, not not+linux.

 -- Josselin Mouette <joss@debian.org>  Sat, 14 Nov 2009 14:47:38 +0100

gnome-user-share (2.28.1-2) unstable; urgency=low

  * Switch to quilt to manage patches.
  * 02_bluetooth_optional.patch: new patch. Make bluetooth support
    optional so that it can build on kFreeBSD.
  * Only require bluetooth stuff on Linux architectures.
  * 03_makefile_error.patch: new patch. Fix error in Makefile.am leading
    to automake being unable to run.
  * 90_relibtoolize.patch: re-run the autotools on top of that.

 -- Josselin Mouette <joss@debian.org>  Sat, 14 Nov 2009 13:32:09 +0100

gnome-user-share (2.28.1-1) unstable; urgency=low

  * New upstream bugfix release.
  * debian/control.in:
    - Build-depend on libcanberra-gtk.

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 01 Nov 2009 14:58:58 +0100

gnome-user-share (2.28.0-1) unstable; urgency=low

  [ Andrea Veri ]
  * New upstream release.
  * debian/control.in:
    - bumped standards-version to latest 3.8.3. No changes
      needed.
    - added a B-D on libgnome-bluetooth-dev as per configure.in
      requirements.
  * debian/copyright:
    - added missing copyright holders.

  [ Luca Falavigna ]
  * debian/copyright:
    - Point to versioned GPL license in common-licenses.

 -- Luca Falavigna <dktrkranz@debian.org>  Sat, 10 Oct 2009 12:53:35 +0200

gnome-user-share (2.26.0-2) unstable; urgency=low

  * Build-depend on scrollkeeper. Closes: #536318.

 -- Josselin Mouette <joss@debian.org>  Sat, 11 Jul 2009 11:27:45 +0200

gnome-user-share (2.26.0-1) unstable; urgency=low

  [ Loic Minier ]
  * Don't rm_conffile /etc/xdg/autostart/gnome-user-share-session.desktop
    during first installation.

  [ Josselin Mouette ]
  * Wrap build-depends and depends.
  * New upstream release. Closes: #530761.
  * 01_apache-config.patch: updated for the new version, removed
    obsolete chunk.
  * Only depend on apache2.2-bin. Kudos to the Apache maintainers for
    making this possible.
  * Explicitly use the path to the worker variant of Apache.
  * Require the package that contains mod_dnssd.
  * Depend on gnome-bluetooth instead of bluez-gnome.
  * Require obex-data-server 0.4 for the new API.
  * Move autostart file to /usr/share/gnome/autostart.
  * preinst: remove the conffile accordingly.

 -- Josselin Mouette <joss@debian.org>  Sat, 27 Jun 2009 11:12:12 +0200

gnome-user-share (0.31-3) unstable; urgency=medium

  * debian/preinst:
    + Remove obsolete conffile.
  * debian/01_apache-config.patch:
    + Fix apache config even more.

 -- Sebastian Dröge <slomo@debian.org>  Fri, 18 Apr 2008 12:42:37 +0200

gnome-user-share (0.31-2) unstable; urgency=low

  * debian/patches/01_apache-config.patch,
    debian/rules:
    + Fix the apache config and don't ship a config for apache 2.0, we only
      support 2.2.

 -- Sebastian Dröge <slomo@debian.org>  Thu, 10 Apr 2008 08:43:56 +0200

gnome-user-share (0.31-1) unstable; urgency=low

  * New upstream release.

 -- Sebastian Dröge <slomo@debian.org>  Mon, 07 Apr 2008 12:23:18 +0200

gnome-user-share (0.30-1) unstable; urgency=low

  * New upstream release:
    + debian/control.in:
      - Depend on obex-data-server and bluez-gnome for ObexFTP file sharing.
      - Update description.
    + debian/copyright:
      - Add Bastien as second upstream author.
    + debian/control.in:
      - Remove glade from build dependencies.
    + debian/patches/01_debianise-dav-user-conf.diff,
      debian/rules:
      - Use the new configure parameter now instead of patching.
    + debian/patches/02_README.diff:
      - Dropped, this is fixed upstream now.

 -- Sebastian Dröge <slomo@debian.org>  Tue, 18 Mar 2008 02:17:30 +0100

gnome-user-share (0.21-1) unstable; urgency=low

  * New upstream release:
    + debian/gnome-user-share-session.*,
      debian/rules:
      - Don't build our own starter, upstream provides one now.
    + debian/control.in:
      - Update build dependencies.
      - Update Standards-Version to 3.7.3, no additional changes needed.

 -- Sebastian Dröge <slomo@debian.org>  Fri, 14 Mar 2008 14:35:18 +0100

gnome-user-share (0.11-3) unstable; urgency=low

  * debian/control:
    + Update to Standards-Version 3.7.3, no additional changes needed.
  * debian/rules:
    + Use LDFLAGS for linking the gnome-user-share-session helper too.

 -- Sebastian Dröge <slomo@debian.org>  Sun, 06 Jan 2008 19:52:14 +0100

gnome-user-share (0.11-2) unstable; urgency=low

  * Upload to unstable

 -- Sebastian Dröge <slomo@debian.org>  Mon, 16 Apr 2007 01:43:08 +0200

gnome-user-share (0.11-1) experimental; urgency=low

  * New upstream release
  * debian/rules,
    debian/control:
    + Use gnome-pkg-tools to add the Gnome team to Uploaders
    + Add get-orig-source target
  * debian/patches/01_debianise-dav-user-conf.diff:
    + Updated for new upstream version

 -- Sebastian Dröge <slomo@debian.org>  Tue,  6 Mar 2007 18:36:22 +0100

gnome-user-share (0.10-4) unstable; urgency=medium

  * Urgency medium because of RC bugfix. This should go to etch.
  * debian/patches/01_debianise-dav-user-conf.diff:
    + Also load the mime module to allow usage of the TypesConfig directive
      (Closes: #411171)

 -- Sebastian Dröge <slomo@debian.org>  Sun, 18 Feb 2007 11:10:21 +0100

gnome-user-share (0.10-3) unstable; urgency=low

  * debian/control:
    + Only build-depend on libselinux-dev on Linux platforms (Closes: #402135)
    + Updated to use my debian.org mail address

 -- Sebastian Dröge <slomo@debian.org>  Mon,  8 Jan 2007 08:27:54 +0100

gnome-user-share (0.10-2) unstable; urgency=low

  * Acknowledge NMU (Closes: #391259)
  * debian/control,
    debian/rules:
    + Add versioned apache2 dependency back but require apache 2.2
  * debian/control:
    + Update Standards-Version to 3.7.2

 -- Sebastian Dröge <slomo@ubuntu.com>  Tue, 17 Oct 2006 17:59:26 +0200

gnome-user-share (0.10-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Don't use a versioned dependency on apache2 (Closes: #391259).

 -- Luk Claes <luk@debian.org>  Sun, 15 Oct 2006 18:16:58 +0200

gnome-user-share (0.10-1) unstable; urgency=low

  * New upstream release
  * debian/patches/01_debianise-dav-user-conf.diff:
    + Update for the new filenames and patch the apache 2.0 and 2.2 configs
  * Depend on apache2 (>= 2.0) and (<< 2.1). We need to rebuild when apache
    2.2 gets into Debian to get the correct config file link for apache
    created.

 -- Sebastian Dröge <slomo@ubuntu.com>  Wed, 19 Apr 2006 15:10:13 +0200

gnome-user-share (0.9-4) unstable; urgency=low

  * Add a autostart .desktop file and a small loader that loads
    gnome-user-share when it was enabled in the capplet. This way
    gnome-user-share is started again with the session when it was enabled
    before.

 -- Sebastian Dröge <slomo@ubuntu.com>  Sun,  2 Apr 2006 13:55:20 +0200

gnome-user-share (0.9-3) unstable; urgency=low

  * Added debian/watch
  * Added README to the package and patch it to include informations about
    avahi and improve the language a bit (02_README.diff) (Closes: #353180)
  * Updated to debhelper compat version 5

 -- Sebastian Dröge <slomo@ubuntu.com>  Thu, 23 Feb 2006 19:51:20 +0100

gnome-user-share (0.9-2) unstable; urgency=low

  * Upload to unstable
  * Add libxt-dev to Build-Depends to fix the failed configure check for X
  * Use --as-needed to get rid of unneeded dependencies

 -- Sebastian Dröge <slomo@ubuntu.com>  Tue, 17 Jan 2006 23:00:03 +0100

gnome-user-share (0.9-1) experimental; urgency=low

  * New upstream release
  * Adjusted Build-Depends for avahi 0.6
  * Upload to Debian (Closes: #283088)

 -- Sebastian Dröge <slomo@ubuntu.com>  Wed, 21 Dec 2005 19:18:04 +0100

gnome-user-share (0.8-1) unstable; urgency=low

  * New upstream release

 -- Sebastian Dröge <slomo@ubuntu.com>  Tue, 15 Nov 2005 23:23:48 +0100

gnome-user-share (0.7-0ubuntu2) dapper; urgency=low

  * Add new FSF address to the copyright file

 -- Sebastian Dröge <slomo@ubuntu.com>  Thu, 10 Nov 2005 07:08:06 +0100

gnome-user-share (0.7-0ubuntu1) dapper; urgency=low

  * New upstream release
  * Use avahi instead of howl and added Build-Depends for it
  * Made Build-Depends stricter

 -- Sebastian Dröge <slomo@ubuntu.com>  Wed,  9 Nov 2005 12:02:33 +0100

gnome-user-share (0.4-1) hoary; urgency=low

  * New upstream release

 -- Jeff Waugh <jeff.waugh@canonical.com>  Sat,  4 Dec 2004 00:05:19 +1100

gnome-user-share (0.3-3) hoary; urgency=low

  * debian/control:
    - Build-Depend on libxt-dev, because autoconf looks for Xt to determine
      if X is installed.
  * debian/patches/01_debianise-dav-user-conf.diff:
    - Only define Max/MinSpareServers and MaxClients when using prefork.

 -- Jeff Waugh <jeff.waugh@canonical.com>  Sat, 27 Nov 2004 01:48:26 +1100

gnome-user-share (0.3-2) hoary; urgency=low

  * Build-Depend on libx11-dev, libice-dev, libsm-dev

 -- Jeff Waugh <jeff.waugh@canonical.com>  Sat, 27 Nov 2004 00:06:14 +1100

gnome-user-share (0.3-1) hoary; urgency=low

  * Initial Release

 -- Jeff Waugh <jdub@perkypants.org>  Tue, 01 Jun 2004 16:14:59 +1000
