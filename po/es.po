# translation of gnome-user-share.HEAD.po to Español
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# 
# Francisco Javier F. Serrador <serrador@cvs.gnome.org>, 2005, 2006.
# Jorge González <jorgegonz@svn.gnome.org>, 200, 2009.
# 
# 
# Daniel Mustieles <daniel.mustieles@gmail.com>, 2011 -2019.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-user-share.HEAD\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-user-share/"
"issues\n"
"POT-Creation-Date: 2019-04-11 16:27+0000\n"
"PO-Revision-Date: 2019-04-23 10:15+0200\n"
"Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>\n"
"Language-Team: es <gnome-es-list@gnome.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 3.32.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/org.gnome.desktop.file-sharing.gschema.xml:5
msgid "When to require passwords"
msgstr "Cuando requerir contraseñas"

#: data/org.gnome.desktop.file-sharing.gschema.xml:6
msgid ""
"When to ask for passwords. Possible values are \"never\", \"on_write\", and "
"\"always\"."
msgstr ""
"Cuando pedir contraseñas. Las opciones posibles son: \"never\" (nunca), "
"\"on_write\" (al escribir) y \"always\" (siempre)."

#: data/gnome-user-share-webdav.desktop.in.in:3
#| msgid "Sharing"
msgid "File Sharing"
msgstr "Compartición de archivos"

#: data/gnome-user-share-webdav.desktop.in.in:4
#| msgid "Launch Personal File Sharing if enabled"
msgid "Launch File Sharing if enabled"
msgstr "Lanzar la compartición de archivos personales si está activada"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/gnome-user-share-webdav.desktop.in.in:6
msgid "share;files;http;network;copy;send;"
msgstr "compartir;archivos;http;red;copiar;enviar;"

#. translators: This is the label for the "Sharing" panel in the Settings
#: src/nautilus-share-bar.c:99
msgid "Sharing"
msgstr "Compartición"

#: src/nautilus-share-bar.c:113
msgid "Sharing Settings"
msgstr "Opciones de compartición"

#: src/share-extension.c:117
msgid ""
"Turn on File Sharing to share the contents of this folder over the network."
msgstr ""
"Active la compartición de archivos para compartir el contenido de esta "
"carpeta en la red."

#. Translators: The %s will get filled in with the user name
#. of the user, to form a genitive. If this is difficult to
#. translate correctly so that it will work correctly in your
#. language, you may use something equivalent to
#. "Public files of %s", or leave out the %s altogether.
#. In the latter case, please put "%.0s" somewhere in the string,
#. which will match the user name string passed by the C code,
#. but not put the user name in the final string. This is to
#. avoid the warning that msgfmt might otherwise generate.
#: src/http.c:124
#, c-format
msgid "%s's public files"
msgstr "Archivos públicos de %s"

#. Translators: This is similar to the string before, only it
#. has the hostname in it too.
#: src/http.c:128
#, c-format
msgid "%s's public files on %s"
msgstr "Archivos públicos de %s en %s"

#~ msgid "Personal File Sharing"
#~ msgstr "Compartición de archivos personales"

#~ msgid "folder-remote"
#~ msgstr "folder-remote"

#~ msgid "Whether Bluetooth clients can send files using ObexPush."
#~ msgstr "Si los clientes Bluetooth pueden enviar archivos usando ObexPush."

#~ msgid ""
#~ "If this is true, Bluetooth devices can send files to the user's Downloads "
#~ "directory when logged in."
#~ msgstr ""
#~ "Si esto es cierto, los dispositivos Bluetooth pueden enviar archivos a la "
#~ "carpeta «Descargas» del usuario cuando haya iniciado la sesión."

#~ msgid "When to accept files sent over Bluetooth"
#~ msgstr "Cuándo aceptar archivos enviados por Bluetooth"

#~ msgid ""
#~ "When to accept files sent over Bluetooth. Possible values are \"always\", "
#~ "\"bonded\" and \"ask\"."
#~ msgstr ""
#~ "Cuándo aceptar archivos enviados por Bluetooth. Los valores posibles son: "
#~ "«always» (siempre), «bonded» (vinculados) y «ask» (preguntar)."

#~ msgid "Whether to notify about newly received files."
#~ msgstr "Si notificar acerca de los nuevos archivos recibidos."

#~| msgid "Launch Personal File Sharing if enabled"
#~ msgid "Launch Bluetooth ObexPush sharing if enabled"
#~ msgstr "Lanzar la compartición Bluetooth ObexPush si está activada"

#~ msgid "Preferences"
#~ msgstr "Preferencias"

#~ msgid "May be used to share or receive files"
#~ msgstr "Se puede usar para compartir o recibir archivos"

#~ msgid "May be shared over the network or Bluetooth"
#~ msgstr "Se puede compartir a través de la red o por Bluetooth"

#~ msgid "May be shared over the network"
#~ msgstr "Se puede compartir a través de la red"

#~ msgid "May be used to receive files over Bluetooth"
#~ msgstr "Se puede usar para recibir archivos por Bluetooth"

#~ msgid "You received \"%s\" via Bluetooth"
#~ msgstr "Ha recibido «%s» a través de Bluetooth"

#~ msgid "You received a file"
#~ msgstr "Recibió un archivo"

#~ msgid "Open File"
#~ msgstr "Abrir archivo"

#~ msgid "Reveal File"
#~ msgstr "Revelar archivo"

#~ msgid "File reception complete"
#~ msgstr "La recepción de archivos está completada"

#~ msgid "You have been sent a file \"%s\" via Bluetooth"
#~ msgstr "Ha enviado un archivo «%s» a través de Bluetooth"

#~ msgid "You have been sent a file"
#~ msgstr "Ha enviado un archivo"

#~ msgid "Receive"
#~ msgstr "Recibir"

#~ msgid "Cancel"
#~ msgstr "Cancelar"

#~ msgid "Share Public directory over the network"
#~ msgstr "Compartir la carpeta «Público» por red"

#~ msgid ""
#~ "If this is true, the Public directory in the users home directory will be "
#~ "shared over the network when the user is logged in."
#~ msgstr ""
#~ "Si esto es cierto, la carpeta «Público» en la carpeta personal de los "
#~ "usuarios se compartirá por red cuando el usuario inicie sesión."

#~ msgid "Share Public directory over Bluetooth"
#~ msgstr "Compartir la carpeta «Público» por Bluetooth"

#~ msgid ""
#~ "If this is true, the Public directory in the users home directory will be "
#~ "shared over Bluetooth when the user is logged in."
#~ msgstr ""
#~ "Si esto es cierto, la carpeta «Público» en la carpeta personal del "
#~ "usuario se compartirá por Bluetooth cuando el usuario inicie sesión."

#~ msgid "Whether to allow Bluetooth clients to write files."
#~ msgstr "Indica si se permite escribir archivos a los clientes por Bluetooth"

#~ msgid ""
#~ "Whether to allow Bluetooth clients to write files, or share the files "
#~ "read-only."
#~ msgstr ""
#~ "Indica si se permite a los clientes por Bluetooth escribir archivos o "
#~ "solamente compartir los archivos de sólo lectura."

#~ msgid ""
#~ "Whether Bluetooth clients need to pair with the computer to send files."
#~ msgstr ""
#~ "Si los clientes Bluetooth necesitan emparejarse con el equipo para enviar "
#~ "archivos."

#~ msgid "Launch Personal File Sharing Preferences"
#~ msgstr "Lanzar las preferencias de compartición de archivos personales"

#~ msgid "Personal File Sharing Preferences"
#~ msgstr "Preferencias de compartición de archivos personales"

#~ msgid "Share Files over the Network"
#~ msgstr "Compartir archivos en la red"

#~ msgid "_Share public files on network"
#~ msgstr "C_ompartir archivos públicos por red"

#~ msgid "_Password:"
#~ msgstr "_Contraseña:"

#~ msgid "_Require password:"
#~ msgstr "_Requerir contraseña:"

#~ msgid "Share Files over Bluetooth"
#~ msgstr "Compartir archivos por Bluetooth"

#~ msgid "Share public files over _Bluetooth"
#~ msgstr "Compartir archivos públicos por _Bluetooth"

#~ msgid "Allo_w remote devices to delete files"
#~ msgstr "_Permitir que los dispositivos remotos eliminen archivos"

#~ msgid "Require re_mote devices to bond with this computer"
#~ msgstr "Requerir que los dispositivos re_motos se vinculen con este equipo"

#~ msgid "Receive Files over Bluetooth"
#~ msgstr "Recibir archivos por Bluetooth"

#~ msgid "Receive files in _Downloads folder over Bluetooth"
#~ msgstr "Recibir los archivos por Bluetooth en la carpeta _Descargas"

#~ msgid "_Accept files: "
#~ msgstr "_Aceptar archivos: "

#~ msgid "_Notify about received files"
#~ msgstr "_Notificar acerca de los archivos recibidos"

#~ msgid "Preferences for sharing of files"
#~ msgstr "Preferencias para la compartición de archivos"

#~ msgid "Unable to launch the Personal File Sharing Preferences"
#~ msgstr ""
#~ "No se pudieron lanzar las preferencias de compartición de archivos "
#~ "personales"

#~ msgid "No reason"
#~ msgstr "Sin razón"

#~ msgid "Could not display the help contents."
#~ msgstr "No se pudo mostrar el contenido de la ayuda."

#~ msgid "Could not build interface."
#~ msgstr "No se pudo construir la interfaz."

#~ msgid "Never"
#~ msgstr "Nunca"

#~ msgid "When writing files"
#~ msgstr "Al escribir archivos"

#~ msgid "Always"
#~ msgstr "Siempre"

#~ msgid "Only for set up devices"
#~ msgstr "Sólo para dispositivos configurados"

#~ msgid "Ask"
#~ msgstr "Preguntar"

#~ msgid "You can share files from this folder and receive files to it"
#~ msgstr "Puede compartir archivos desde esta carpeta y recibirlos en ella"

#~ msgid "You can receive files over Bluetooth into this folder"
#~ msgstr "Puede recibir archivos a través de Bluetooth en esta carpeta"

#~ msgid "Please log in as the user guest"
#~ msgstr "Inicie sesión como usuario invitado"

#~ msgid "Only for Bonded and Trusted devices"
#~ msgstr "Sólo para dispositivos vinculados y de confianza"

#~ msgid "gtk-help"
#~ msgstr "gtk-help"
